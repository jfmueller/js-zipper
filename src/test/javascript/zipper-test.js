describe("zipper", function() {
	var t1 = treeItem(3, {id: 1});
	var t2 = treeItem(5, {id: 2});
	var t3 = minus(t1, t2, 3); //t3 = 3 - 5

	it("should go down", function() {
		var l1 = loc(t3, zipperPath()).goDown();
		expect(l1.tree().evaluate()).toBe(3);
		expect(l1.path().args().id).toBe(3);
	});
	it("should identify leafs", function() {
		var l1 = loc(t3, zipperPath()).goDown();
		expect(l1.isLeaf()).toBe(true);
	});
	it("should go right", function() {
		var l = loc(t3, zipperPath()).goDown().goRight();
		expect(l.tree().evaluate()).toBe(5);
		expect(l.path().args().id).toBe(3);
	});
	it ("should identify last item in a row", function() {
		var l = loc(t3, zipperPath()).goDown().goRight();
		expect(l.isLast()).toBe(true);
	});
	it("should go left", function() {
		var l = loc(t3, zipperPath()).goDown().goRight().goLeft();
		expect(l.tree().evaluate()).toBe(3);
	});
	it("should identify first item in a row", function() {
		var l = loc(t3, zipperPath()).goDown().goRight().goLeft();
		expect(l.isFirst()).toBe(true);
	});
	it("should go up", function() {
		var l = loc(t3, zipperPath()).goDown().goUp();
		expect(l.tree().evaluate()).toBe(-2);
		expect(l.path().id).toBe(undefined);
		var l2 = loc(t3, zipperPath()).goDown().goUp().goDown().goUp();
		expect(l2.tree().evaluate()).toBe(-2);
		var t4 = plus(treeItem(5), t3, 4);//5 + (3-5)
		var l3 = loc(t4, zipperPath()).goDown().goRight().goDown().goUp();
		expect(l3.tree().evaluate()).toBe(-2);
		expect(l3.goDown().tree().evaluate()).toBe(3);
		expect(l3.path().args().id).toBe(4);
	});
	it("should identify top location", function() {
		expect(zipperPath().isTop()).toBe(true);
		var l = loc(t3, zipperPath()).goDown().goUp();
		expect(l.isRoot()).toBe(true);
	});
	it("should go down and up again", function() {
		var l1 = loc(t3, zipperPath()).goDown().goUp();
		expect(l1.tree().evaluate()).toBe(-2);
	});
	it("should go down, right, up", function() {
		var t = treeItem(3, {id:3}); 
		var t2 = plus(treeItem(2, {id:1}), t, 4);
		var t3 = plus(treeItem(1, {id:2}), t2, 5);
		var l2 = loc(t3, zipperPath()).goDown().goRight().goLeft().goUp();
		expect(t3.asString()).toBe("(1 + (2 + 3))");
		expect(l2.tree().asString()).toBe("(1 + (2 + 3))");
		expect(l2.tree().args().id).toBe(5);
	});
	it("should go down, right, down", function() {
		var t = treeItem(3, {id:3}); 
		var t2 = plus(treeItem(2, {id:1}), t, 4);
		var t3 = plus(treeItem(1, {id:2}), t2, 5);
		var l2 = loc(t3, zipperPath()).goDown().goRight().goDown();
		expect(l2.tree().asString()).toBe(2);
	})
});