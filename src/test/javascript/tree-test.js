describe("Tree item", function() {
	it("should return correct item", function() {
		expect(treeItem(2).evaluate()).toBe(2);
	});
	it("should keep its id", function() {
		expect(treeItem("a", {id : 2}).id()).toBe(2);
	});
	it("should return its value as string", function() {
		expect(treeItem("a").asString()).toBe("a");
	});
})

describe("Tree section", function() {
	it("should return correct children", function() {
		var children = list(treeItem(1), list(treeItem(2)));
		var section = treeSection(children);
		expect(section.treeSectionChildren().count()).toBe(2);
		expect(section.treeSectionChildren().head().evaluate()).toBe(1);
	});
	it("should perform evaluate function", function() {
		var f = function() { return "evaluated"; };
		expect(treeSection(list(), {operation: f} ).evaluate()).toBe("evaluated");
	});
	it("should count elements correctly", function() {
		expect(treeItem("a").count()).toBe(1);
		expect(treeSection(list()).count()).toBe(1);
		expect(treeSection(list(treeItem(1), list(treeItem(2)))).count()).toBe(3);
	});
	it("should keep its id", function() {
		expect(treeSection("a", {id : 2}).id()).toBe(2);
	});
	it("should perform the asString operation if asString is called", function() {
		expect(treeSection("a", { asString : function() { return "s";}}).asString()).toBe("s");
	});
});

describe("plus", function() {
	it("should add its elements", function() {
		expect(plus(treeItem(1), treeItem(4)).evaluate()).toBe(5);
	});
	it("should return zero on an empty tree", function() {
		expect(plus().evaluate()).toBe(0);
	});
	it("should add its subelements", function() {
		expect(plus(treeItem(1), plus(treeItem(2), treeItem(5))).evaluate()).toBe(8);
	});
	it("should keep its id", function() {
		expect(plus(treeItem(1), treeItem(2), 5).id()).toBe(5);
	});
	it("should render itself with a plus sign", function() {
		expect(plus(treeItem(1), treeItem(2)).asString()).toBe("(1 + 2)")
	});
});

describe("minus", function() {
	it("should subtract its second element from its first element", function() {
		expect(minus(treeItem(5), treeItem(3)).evaluate()).toBe(2);
	});
	it("should handle priorities correctly", function() {
		expect(minus(treeItem(10), plus(treeItem(3), treeItem(5))).evaluate()).toBe(2); //10 - (3 + 5)
	});
	it("should keep its id", function() {
		expect(minus(treeItem(1), treeItem(2), 5).id()).toBe(5);
	});
	it("should render itself with a minus sign", function() {
		expect(minus(treeItem(1), treeItem(2)).asString()).toBe("(1 - 2)")
	});
});

describe("The multiply function", function() {
	it("should multiply its elements", function() {
		expect(multiply(treeItem(5), treeItem(2)).evaluate()).toBe(10);
	});
	it("should keep its id", function() {
		expect(multiply(treeItem(1), treeItem(2), 5).id()).toBe(5);
	});
	it("should render itself with a star sign", function() {
		expect(multiply(treeItem(1), treeItem(2)).asString()).toBe("(1 * 2)")
	});
})