describe("functional list", function() {
	it("should be empty if it is empty", function() {
		expect(list().isEmpty()).toBe(true);
	});
	it("should return correct count for empty lists", function() {
		expect(list().count()).toBe(0);
	});
	it("should correctly count elements in a list", function() {
		expect(list(1, list(2, list())).count()).toBe(2);
		expect(list(1, list(2)).count()).toBe(2);
	});
	it("should return head of a list", function() {
		expect(list(1, list(2)).head()).toBe(1);
	});
	it("should return tail of a list", function() {
		expect(list(1, list(2)).tail().head()).toBe(2);		
	});
	it("should return the reverse of a list", function() {
		expect(list(1).reverse().head()).toBe(1);
		expect(list(1, list(2)).reverse().count()).toBe(2);
		expect(list(1, list(2)).reverse().head()).toBe(2);
		expect(list(1, list(2)).reverse().tail().head()).toBe(1);
	});
	it("should append a list to another list", function() {
		var l1 = list(1, list(2));
		var l2 = list(3, list(4));
		expect(l1.append(l2).count()).toBe(4);
		expect(l1.append(l2).reverse().head()).toBe(4);
	});
	it("should append a more complicated list to another list", function() {
		var l1 = list(1, list(2));
		var l2 = list(4, list(5));
		var l3 = l1.reverse().append(list(3, l2));
		expect(l3.count()).toBe(5);
		expect(l3.reverse().head()).toBe(5);
	});
	it("should execute the aggregate function correctly", function() {
		var sum = function(initial,item) {
			return item + initial;
		};
		expect(list(1,list(2)).aggregate(sum,0)).toBe(3);
	});
	it("should convert a list to an array", function() {
		var l = list(1, list(2, list(3)));
		expect(l.toArray().length).toBe(3);
		expect(l.toArray()[0]).toBe(3);
		expect(l.reverse().toArray()[0]).toBe(1);
	})
});