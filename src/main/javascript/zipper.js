function list(head, tail) {
	var result = { }

	result.head = function() {
		return head;
	};

	result.tail = function() {
		return tail;
	};

	result.isEmpty = function() {
		return (head == undefined);
	};

	result.append = function(list2) {
		if (head == undefined)
			return list2; 
		if (tail == undefined)
			return list(head, list2);
		else
			return list(head, tail.append(list2));
	};

	result.reverse = function() {
		if (head == undefined)
			return list();
		if (tail == undefined)
			return list(head);
		else
			return tail.reverse().append(list(head));
	};

	result.aggregate = function(aggregator,initial) {
		if (head == undefined)
			return initial;
		if (tail == undefined)
			return aggregator(initial, head);
		return tail.aggregate(aggregator,aggregator(initial,head));
	}

	result.count = function() {
		if (head == undefined)
			return 0;
		if (tail == undefined)
			return 1;
		return 1 + tail.count();
	}

	result.toArray = function() {
		if (head == undefined)
			return [];
		if (tail == undefined)
			return [head];
		var result = tail.toArray();
		result.push(head);
		return result;
	}

	return result;
}

function treeItem(item, inArgs) {
	if (inArgs == undefined)
		return treeItem(item, {});
	var args = {
		operation : function() {
			return item;
		},
		id : inArgs.id,
		asString : function() {
			return item;
		}
	};
	return treeSection(list(), args);
}
function treeSection(children, args) {
	if (args == undefined)
		return treeSection(children, {});
	var result = {};
	result.treeSectionChildren = function() {
		return children;
	}
	result.isItem = function() {
		return children.count() === 0;
	}
	result.evaluate = function() {
		return args.operation(children);
	}
	result.count = function() {
		return children.aggregate(function(initial,head) {
			return initial + head.count();
		}, 1)
	}
	result.operation = function() {
		return args.operation;
	}
	result.id = function() {
		return args.id;
	}
	result.asString = function() {
		return args.asString();
	}
	result.args = function() {
		return args;
	}
	return result;
}

function plus(left, right, id) {
	var args = {
			operation : function(ch) {
				if (ch == undefined)
					return 0;
				if (ch.head() == undefined)
					return 0;
				if (ch.tail() == undefined || ch.tail().isEmpty())
					return ch.head().evaluate;
				return ch.head().evaluate() + ch.tail().head().evaluate();
				},
			id : id,
			asString : function() {
				return "(" + left.asString() + " + " + right.asString() + ")";
				}
		};
	return treeSection(list(left, list(right)), args)
};

function minus(left, right, id) {
	var args = {
		operation : function(ch) {
			if(ch == undefined)
				return 0;
			if(ch.head() == undefined)
				return 0;
			if(ch.tail() == undefined || ch.tail().isEmpty())
				return ch.head().evaluate();
			return ch.head().evaluate() - ch.tail().head().evaluate();
			},
		id : id,
		asString : function() {
		return "(" + left.asString() + " - " + right.asString() + ")";
			}
		};
	return treeSection(list(left, list(right)), args);
};

function multiply(left, right, id) {
	var args = {
		operation : function(ch) {
			if(ch == undefined)
				return 0;
			if(ch.head() == undefined)
				return 0;
			if(ch.tail() == undefined || ch.tail().isEmpty())
				return ch.head().evaluate();
			return ch.head().evaluate() * ch.tail().head().evaluate();
		},
		id : id,
		asString : function() {
			return "(" + left.asString() + " * " + right.asString() + ")";
		}
	};
	return treeSection(list(left, list(right)), args);
};


//In addition to the original specification, we also store the args in our path.
//This is necessary to restore the tree later on
function zipperPath(treesLeft, parentPath, treesRight, args) {
	var result = {};

	result.left = function() {
		return treesLeft || list();
	}
	result.parentPath = function() {
		return parentPath;
	}
	result.right = function() {
		return treesRight || list();
	}
	result.args = function() {
		return args;
	}
	result.isTop = function() {
		return treesLeft == undefined && 
			parentPath == undefined &&
			treesRight == undefined;
	}
	return result;
};

function loc(t, p) {
	var result = {};

	result.tree = function() {
		return t;
	};
	result.path = function() {
		return p;
	};
	result.goLeft = function() {
		if (p.isTop())
			throw "left of top";
		var left = p.left();
		if (left.isEmpty())
			throw "left of first";
		var up = p.parentPath();
		var right = p.right();
		return loc(left.head(), zipperPath(left.tail(), up, list(t, right), p.args()));
	};
	result.goRight = function() {
		if (p.isTop())
			throw "right of top";
		var left = p.left(),
			right = p.right(),
			up = p.parentPath();
		if (right.isEmpty())
			throw "right of last";
		return loc(right.head(), zipperPath(list(t, left), up, right.tail(), p.args()));
	};
	result.goUp = function() {
		if (p.isTop())
			throw "up of top";
		var left = p.left(),
			right = p.right(),
			up = p.parentPath();
		var args = p.args();
		return loc(treeSection(left.reverse().append(list(t, right)), args), up);
	};
	result.goDown = function() {
		if (t.isItem())
			throw "down of item";
		var tt = t.treeSectionChildren();
		if (tt.isEmpty())
			throw "down of empty";
		return loc(tt.head(), zipperPath(list(), p, tt.tail(), t.args()));
	};
	result.isRoot = function() {
		return p.isTop();
	}
	result.isLeaf = function() {
		return t.isItem();
	};
	result.isLast = function() {
		return p.right().isEmpty();
	};
	result.isFirst = function() {
		return p.left().isEmpty();
	}
	return result;
};