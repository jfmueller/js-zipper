js-zipper
=========

Introduction
------------
Zippers are a data type and associated operations. They allow for cheap navigating of tree structures. (They can also be generalised to navigate arbitrary data structures).

License
-------
MIT License (see LICENSE file)

Documentation
-------------
please refer to the test files in src/test/javascript for some examples

How to run the tests
--------------------
Install maven. Then
a) run "mvn jasmine:bdd" and open http://localhost:8234 or
b) run "mvn test"

See also
--------
http://www.st.cs.uni-saarland.de/edu/seminare/2005/advanced-fp/docs/huet-zipper.pdf